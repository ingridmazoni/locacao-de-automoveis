package pojo;

import java.sql.Date;

public class Locacao {
	private Automoveis automovel;
	private Cliente cliente;
	private String dataLocacao;
	private String dataDevolucao;
	private double valorTotal;
	
		
	public Automoveis getAutomovel() {
		return automovel;
	}
	public void setAutomovel(Automoveis automovel) {
		this.automovel = automovel;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public String getDataLocacao() {
		return dataLocacao;
	}
	public void setDataLocacao(String dataLocacao) {
		this.dataLocacao = dataLocacao;
	}
	public String getDataDevolucao() {
		return dataDevolucao;
	}
	public void setDataDevolucao(String dataDevolucao) {
		this.dataDevolucao = dataDevolucao;
	}
	public double getValorTotal() {
		return valorTotal;
	}
	public void setValorTotal(double valorTotal) {
		this.valorTotal = valorTotal;
	}
	
	
	
	
	

}
