package pojo;

public class Automoveis {
	private int CodAutomovel;
	private String marca;
	private String modelo;
	private String cor;
	private int ano;
	private double motor;
	private Tipo tipo;
	
	public int getCodAutomovel() {
		return CodAutomovel;
	}
	public void setCodAutomovel(int codAutomovel) {
		CodAutomovel = codAutomovel;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getCor() {
		return cor;
	}
	public void setCor(String cor) {
		this.cor = cor;
	}
	public int getAno() {
		return ano;
	}
	public void setAno(int ano) {
		this.ano = ano;
	}
	
	
	public double getMotor() {
		return motor;
	}
	public void setMotor(double motor) {
		this.motor = motor;
	}
	public Tipo getTipo() {
		return tipo;
	}
	public void setTipo(Tipo tipo) {
		this.tipo = tipo;
	}
	
	
	
	
	
	
	

}
