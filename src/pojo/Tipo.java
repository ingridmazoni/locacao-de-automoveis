package pojo;

public class Tipo {
	private int CodTipo;
	private String tipo;
	
	public int getCodTipo() {
		return CodTipo;
	}
	public void setCodTipo(int codTipo) {
		CodTipo = codTipo;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

}
