package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.sql.Date;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.text.MaskFormatter;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JFormattedTextField;
import javax.swing.JRadioButton;

import pojo.Automoveis;
import pojo.Cliente;
import javax.swing.JButton;

import dao.AutomoveisDao;
import dao.ClienteDao;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class TelaCliente extends JFrame {

	private JPanel contentPane;
	private JTextField txtnome;
	private JTextField txtendereco;
	private JTextField txtCNH;
	private JFormattedTextField txtdatanasc;
	private JFormattedTextField txttelefone;
	private JFormattedTextField txtcpf;
	private JRadioButton rdPesquisar;
	private JRadioButton rdAtualizar;
	private JRadioButton rdExcluir;
	private JRadioButton rdCadastrar;
	private MaskFormatter data;
	private JButton btnEditar;
	private JButton btnApagar;
	private JButton btnInserir;
	private JButton btnPesquisar;
	private JRadioButton rdLimpar;
	private JTable tabelaCliente;
	private DefaultTableModel modelo;

	
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaCliente frame = new TelaCliente();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws ParseException 
	 */
	public TelaCliente() throws ParseException {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent arg0) {
				atualizaTabela();
				perfilPadrao();
			}
		});
		setTitle("Cadastro Cliente");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 711, 669);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblCpf = new JLabel("CPF:");
		lblCpf.setBounds(29, 25, 46, 14);
		contentPane.add(lblCpf);
		
		MaskFormatter cpf=new MaskFormatter("###.###.###-##");
		txtcpf = new JFormattedTextField(cpf);
		txtcpf.setEditable(false);
		txtcpf.setBounds(74, 22, 130, 20);
		contentPane.add(txtcpf);
		
		JLabel lblCnh = new JLabel("CNH:");
		lblCnh.setBounds(29, 66, 46, 14);
		contentPane.add(lblCnh);
		
		txtCNH = new JTextField();
		txtCNH.setEditable(false);
		txtCNH.setBounds(74, 63, 203, 20);
		contentPane.add(txtCNH);
		
		
		JLabel lblNome = new JLabel("Nome:");
		lblNome.setBounds(29, 112, 46, 14);
		contentPane.add(lblNome);
		
		txtnome = new JTextField();
		txtnome.setEditable(false);
		txtnome.setBounds(75, 109, 202, 20);
		contentPane.add(txtnome);
		
		
		JLabel lblEndereo = new JLabel("Endere\u00E7o ");
		lblEndereo.setBounds(29, 160, 64, 14);
		contentPane.add(lblEndereo);
		
		txtendereco = new JTextField();
		txtendereco.setEditable(false);
		txtendereco.setBounds(91, 157, 186, 20);
		contentPane.add(txtendereco);
		txtendereco.setColumns(10);
		
		JLabel lblTelefone = new JLabel("Telefone:");
		lblTelefone.setBounds(29, 212, 46, 14);
		contentPane.add(lblTelefone);
		
		MaskFormatter telefone=new MaskFormatter("(##)####-####");
		txttelefone = new JFormattedTextField(telefone);
		txttelefone.setEditable(false);
		txttelefone.setBounds(91, 209, 169, 20);
		contentPane.add(txttelefone);
		
		data=new MaskFormatter("##/##/####");
		JLabel lblDataNascimento = new JLabel("Data Nascimento");
		lblDataNascimento.setBounds(29, 261, 96, 14);
		contentPane.add(lblDataNascimento);
		
		txtdatanasc = new JFormattedTextField(data);
		txtdatanasc.setEditable(false);
		txtdatanasc.setBounds(131, 258, 146, 20);
		contentPane.add(txtdatanasc);
		
		rdCadastrar = new JRadioButton("Adicionar");
		rdCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				perfilCadastrar();
			}
		});
		rdCadastrar.setBounds(431, 25, 109, 23);
		contentPane.add(rdCadastrar);
		
		rdExcluir = new JRadioButton("Remover");
		rdExcluir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				perfilExcluir();
			}
		});
		rdExcluir.setBounds(431, 62, 109, 23);
		contentPane.add(rdExcluir);
		
		rdAtualizar = new JRadioButton("Editar");
		rdAtualizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				perfilAtualizar();
			}
		});
		rdAtualizar.setBounds(431, 108, 109, 23);
		contentPane.add(rdAtualizar);
		
		rdPesquisar = new JRadioButton("Pesquisar");
		rdPesquisar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
					perfilPesquisar();
				
			}
		});
		rdPesquisar.setBounds(431, 156, 109, 23);
		contentPane.add(rdPesquisar);
		
		rdLimpar = new JRadioButton("Limpar");
		rdLimpar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				perfilPadrao();
			}
		});
		rdLimpar.setBounds(431, 208, 109, 23);
		contentPane.add(rdLimpar);
		
		ButtonGroup grupo=new ButtonGroup();
		grupo.add(rdAtualizar);
		grupo.add(rdPesquisar);
		grupo.add(rdCadastrar);
		grupo.add(rdExcluir);
		grupo.add(rdLimpar);
		
		
		btnInserir = new JButton("Inserir");
		btnInserir.setVisible(false);
		btnInserir.setBounds(36, 344, 89, 23);
		contentPane.add(btnInserir);
		
		btnEditar = new JButton("Editar");
		btnEditar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				atualizar();
			}
		});
		btnEditar.setVisible(false);
		btnEditar.setBounds(149, 344, 89, 23);
		contentPane.add(btnEditar);
		
		btnApagar = new JButton("Apagar");
		btnApagar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				exclui();
			}
		});
		btnApagar.setVisible(false);
		btnApagar.setBounds(259, 344, 89, 23);
		contentPane.add(btnApagar);
		
		btnPesquisar = new JButton("Pesquisar");
		btnPesquisar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				pesquisar();
			}
		});
		btnPesquisar.setVisible(false);
		btnPesquisar.setBounds(364, 344, 109, 23);
		contentPane.add(btnPesquisar);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(45, 412, 627, 191);
		contentPane.add(scrollPane);
		
		tabelaCliente = new JTable();
		modelo=new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"CPF", "CNH", "Nome", "Endere\u00E7o", "Telefone", "Nascimento", "Idade"
			}
		);
		tabelaCliente.setModel(modelo);
		scrollPane.setViewportView(tabelaCliente);
		
		
		
		btnInserir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				adiciona();
				
			}
		});
		
		
		
	}
	
		
	public Cliente getCliente(){
		Cliente cliente=new Cliente();
		
		cliente.setCpf(txtcpf.getText());
		cliente.setCnh(txtCNH.getText());
		cliente.setNome(txtnome.getText());
		cliente.setEndereco(txtendereco.getText());
		cliente.setTelefone(txttelefone.getText());
		cliente.setDataNascimento(txtdatanasc.getText());
		
		return cliente;
				
	}
	
	public void setCliente(Cliente cliente){
		txtcpf.setText(cliente.getCpf());
		txtCNH.setText(String.valueOf(cliente.getCnh()));
		txtnome.setText(cliente.getNome());
		txtendereco.setText(cliente.getEndereco());
		txttelefone.setText(cliente.getTelefone());
		txtdatanasc.setText(cliente.getDataNascimento());
	}
	
	
	public void adiciona() {
		ClienteDao cliDao=new ClienteDao();
		Cliente c;
		
		try {
			c=getCliente();
			c.setCpf(txtcpf.getText());
			cliDao.insereCliente(c);
			atualizaTabela();
			limpar();
			perfilPadrao();
			
			
		} catch (NumberFormatException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null,"Os dados foram digitados incorretamante","Erro",JOptionPane.ERROR_MESSAGE);
			
		}
		catch (SQLException e) {
			JOptionPane.showMessageDialog(null, e.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
		}
		
	}
	
	public void exclui(){
		ClienteDao cliDao=new ClienteDao();
		Cliente c=new Cliente();
		
		try{

			c.setCpf(txtcpf.getText());
			cliDao.excluiCliente(c);
			atualizaTabela();
			limpar();
			perfilPadrao();
		
		}
		catch(NumberFormatException e){
			e.printStackTrace();
			JOptionPane.showMessageDialog(null,"Os dados foram digitados incorretamante","Erro",JOptionPane.ERROR_MESSAGE);
			
		}
		catch (SQLException e) {
			JOptionPane.showMessageDialog(null, e.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
		}
		
		
		
		
		
		
	}
		
	public void atualizar(){
		ClienteDao cliDao=new ClienteDao();
		Cliente c;
		try {
			c=getCliente();
			c.setCpf(txtcpf.getText());
			cliDao.atualizaCliente(c);
			atualizaTabela();
			limpar();
			perfilPadrao();
	
			
		} catch (NumberFormatException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null,"Os dados foram digitados incorretamante","Erro",JOptionPane.ERROR_MESSAGE);
			
		}
		catch (SQLException e) {
			JOptionPane.showMessageDialog(null, e.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
		}
		
	}
	
	
	public void pesquisar(){
		ClienteDao cliDao=new ClienteDao();
		Cliente cliente=new Cliente();
		
		try {
			
			cliente.setCpf(txtcpf.getText());
			setCliente(cliDao.consultaCliente(cliente));
			
		} catch (NullPointerException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null,"O cliente n�o foi localizado","Erro",JOptionPane.ERROR_MESSAGE);
			
		}
		catch(NumberFormatException e){
			e.printStackTrace();
			JOptionPane.showMessageDialog(null,"Para fazer a pesquisa digite o cpf no formato XXX.XXX.XXX-XX","Erro",JOptionPane.ERROR_MESSAGE);
		}
		catch (SQLException e) {
			JOptionPane.showMessageDialog(null, e.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
		}
		
	}
	
	public List<Cliente> listaClientes(){
		ClienteDao cliDao=new ClienteDao();
		List<Cliente> lista=new ArrayList<Cliente>();
		lista=cliDao.consultaListaCliente();
		return lista;
	}
	
	
	public void atualizaTabela(){
		List<Cliente> lista=listaClientes();
		
		if(modelo.getRowCount()!=0){
			modelo.setRowCount(0);
		}
		if(lista!=null){
			for(Cliente p:lista){
				Object[] objeto=new Object[7];
				objeto[0]=p.getCpf();
				objeto[1]=p.getCnh();
				objeto[2]=p.getNome();
				objeto[3]=p.getEndereco();
				objeto[4]=p.getTelefone();
				objeto[5]=p.getDataNascimento();
				objeto[6]=p.getIdade();
				modelo.addRow(objeto);
				
			}
		}
		
	}
	
	public void perfilCadastrar(){
		txtcpf.setEditable(true);
		txtCNH.setEditable(true);
		txtnome.setEditable(true);
		txtendereco.setEditable(true);
		txttelefone.setEditable(true);
		txtdatanasc.setEditable(true);
		btnEditar.setVisible(false);
		btnApagar.setVisible(false);
		btnInserir.setVisible(true);
		btnPesquisar.setVisible(false);
		
	}
	
	
	public void perfilAtualizar(){
		txtcpf.setEditable(false);
		txtCNH.setEditable(true);
		txtnome.setEditable(true);
		txtendereco.setEditable(true);
		txttelefone.setEditable(true);
		txtdatanasc.setEditable(true);
		btnEditar.setVisible(true);
		btnApagar.setVisible(false);
		btnInserir.setVisible(false);
		btnPesquisar.setVisible(false);
	}
	
	
	public void perfilExcluir(){
		txtcpf.setEditable(false);
		txtCNH.setEditable(false);
		txtnome.setEditable(false);
		txtendereco.setEditable(false);
		txttelefone.setEditable(false);
		txtdatanasc.setEditable(false);
		btnEditar.setVisible(false);
		btnApagar.setVisible(true);
		btnInserir.setVisible(false);
		btnPesquisar.setVisible(false);
	}
	
	public void perfilPesquisar(){
		txtcpf.setEditable(true);
		txtCNH.setEditable(false);
		txtnome.setEditable(false);
		txtendereco.setEditable(false);
		txttelefone.setEditable(false);
		txtdatanasc.setEditable(false);
		btnEditar.setVisible(false);
		btnApagar.setVisible(false);
		btnInserir.setVisible(false);
		btnPesquisar.setVisible(true);
	}
	
	public void perfilPadrao(){
		txtcpf.setEditable(false);
		txtCNH.setEditable(false);
		txtnome.setEditable(false);
		txtendereco.setEditable(false);
		txttelefone.setEditable(false);
		txtdatanasc.setEditable(false);
		btnEditar.setVisible(false);
		btnApagar.setVisible(false);
		btnInserir.setVisible(false);
		btnPesquisar.setVisible(false);
		
	}
	
	
	public void limpar(){
		txtcpf.setText("");
		txtCNH.setText("");
		txtnome.setText("");
		txtendereco.setText("");
		txttelefone.setText("");
		txtdatanasc.setText("");
		
	}
}
