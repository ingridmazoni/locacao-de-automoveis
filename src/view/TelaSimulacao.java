package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.KeyboardFocusManager;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.text.MaskFormatter;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JFormattedTextField;
import javax.swing.SwingConstants;
import javax.swing.JButton;

import dao.*;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import pojo.*;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.data.JRXmlDataSource;
import javax.swing.JComboBox;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class TelaSimulacao extends JFrame {

	private JPanel contentPane;
	private JTextField txtNomeCliente;
	private JTextField txtValorTotal;
	private final JFormattedTextField txtCpfCliente;
	private JFormattedTextField txtDataLocacao;
	private JFormattedTextField txtDataDevolucao;
	private JComboBox cmbCodigoAutomovel;
	private JButton btnGerarRelatorio;
	private JButton btnCadastrarSimulacao ;
	private JButton btnLimpar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaSimulacao frame = new TelaSimulacao();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws ParseException 
	 */
	public TelaSimulacao() throws ParseException {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent arg0) {
				preencheComboAutomovel();
			}
		});
		setTitle("Simula\u00E7\u00E3o de Loca\u00E7\u00E3o");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 701, 446);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblCpfCliente = new JLabel("CPF Cliente");
		lblCpfCliente.setHorizontalAlignment(SwingConstants.CENTER);
		lblCpfCliente.setBounds(10, 42, 85, 14);
		contentPane.add(lblCpfCliente);
		
		JLabel lblNomeCliente = new JLabel("Nome Cliente ");
		lblNomeCliente.setHorizontalAlignment(SwingConstants.CENTER);
		lblNomeCliente.setBounds(10, 79, 95, 14);
		contentPane.add(lblNomeCliente);
		
		txtNomeCliente = new JTextField();
		txtNomeCliente.setEditable(false);
		txtNomeCliente.setBounds(115, 76, 293, 20);
		contentPane.add(txtNomeCliente);
		txtNomeCliente.setColumns(10);
		
		MaskFormatter cpf=new MaskFormatter("###.###.###-##");
		
		
		txtCpfCliente = new JFormattedTextField(cpf);
		txtCpfCliente.setFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS, Collections.EMPTY_SET);
		txtCpfCliente.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent a) {
				if(a.getKeyCode()==KeyEvent.VK_ENTER||a.getKeyCode()==KeyEvent.VK_TAB){
					buscaCliente();
				}
			}
		});
		txtCpfCliente.setBounds(105, 39, 129, 20);
		contentPane.add(txtCpfCliente);
		
		JLabel lblDataLocao = new JLabel("Data Loca\u00E7\u00E3o");
		lblDataLocao.setHorizontalAlignment(SwingConstants.CENTER);
		lblDataLocao.setBounds(20, 176, 85, 14);
		contentPane.add(lblDataLocao);
		
		MaskFormatter data=new MaskFormatter("##/##/####");
		txtDataLocacao = new JFormattedTextField(data);
		txtDataLocacao.setBounds(110, 173, 115, 20);
		contentPane.add(txtDataLocacao);
		
		JLabel lblDataDevoluo = new JLabel("Data Devolu\u00E7\u00E3o");
		lblDataDevoluo.setHorizontalAlignment(SwingConstants.CENTER);
		lblDataDevoluo.setBounds(246, 176, 93, 14);
		contentPane.add(lblDataDevoluo);
		
		
		txtDataDevolucao = new JFormattedTextField(data);
		txtDataDevolucao.setFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS, Collections.EMPTY_SET);
		txtDataDevolucao.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent a) {
				if(a.getKeyCode()==KeyEvent.VK_ENTER||a.getKeyCode()==KeyEvent.VK_TAB){
					SimulacaoDao simulacao=new SimulacaoDao();
					
					try {
						txtValorTotal.setText(String.valueOf(simulacao.simulaLocacao(getSimulacao()).getValorTotal()));
						perfilLocacao();
					} catch (SQLException e) {
						JOptionPane.showMessageDialog(null, e.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
						e.printStackTrace();
					}
				}
			}
		});
		
		
		
		txtDataDevolucao.setBounds(349, 173, 123, 20);
		contentPane.add(txtDataDevolucao);
		
		JLabel lblValorTotal = new JLabel("Valor Total ");
		lblValorTotal.setHorizontalAlignment(SwingConstants.CENTER);
		lblValorTotal.setBounds(20, 232, 85, 14);
		contentPane.add(lblValorTotal);
		
		txtValorTotal = new JTextField();
		txtValorTotal.setEditable(false);
		txtValorTotal.setBounds(115, 229, 112, 20);
		contentPane.add(txtValorTotal);
		txtValorTotal.setColumns(10);
		
		btnGerarRelatorio = new JButton("Gerar Relat\u00F3rio");
		btnGerarRelatorio.setEnabled(false);
		btnGerarRelatorio.setBounds(524, 89, 149, 23);
		contentPane.add(btnGerarRelatorio);
		
		btnCadastrarSimulacao = new JButton("Cadastrar Simula\u00E7\u00E3o");
		btnCadastrarSimulacao.setEnabled(false);
		btnCadastrarSimulacao.setBounds(529, 137, 144, 23);
		contentPane.add(btnCadastrarSimulacao);
		
		JLabel lblEscolhaOVeculo = new JLabel("Escolha o Ve\u00EDculo");
		lblEscolhaOVeculo.setHorizontalAlignment(SwingConstants.CENTER);
		lblEscolhaOVeculo.setBounds(22, 125, 107, 14);
		contentPane.add(lblEscolhaOVeculo);
		
		cmbCodigoAutomovel = new JComboBox();
		cmbCodigoAutomovel.setBounds(139, 122, 354, 20);
		contentPane.add(cmbCodigoAutomovel);
		
		btnLimpar = new JButton("Limpar");
		btnLimpar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				perfilCadastraLocacao();
			}
		});
		btnLimpar.setBounds(529, 187, 144, 23);
		contentPane.add(btnLimpar);
	}
	
	
	public Locacao getSimulacao(){
		Locacao loc=new Locacao();
		Cliente cli=new Cliente();
		Automoveis auto=new Automoveis();
	
		cli.setCpf(txtCpfCliente.getText());
		cli.setNome(txtNomeCliente.getText());
		loc.setCliente(cli);
		
		String texto=cmbCodigoAutomovel.getSelectedItem().toString();
		String codigoAuto=texto.substring(0,texto.lastIndexOf("-"));
		auto.setCodAutomovel(Integer.parseInt(codigoAuto));
		loc.setAutomovel(auto);
		
		loc.setDataDevolucao(txtDataDevolucao.getText());
		loc.setDataLocacao(txtDataLocacao.getText());
		
		return loc;
		
	}
	
	
	public void setSimulacao(Locacao locacao){
		
		txtCpfCliente.setText(locacao.getCliente().getCpf());
		txtNomeCliente.setText(locacao.getCliente().getNome());
		
		txtDataLocacao.setText(locacao.getDataLocacao());
		txtDataDevolucao.setText(locacao.getDataDevolucao());
		txtValorTotal.setText(String.valueOf(locacao.getValorTotal()));
		
	}
	
	
	public void gerarRelatorio(Locacao locacao){ 
		
	//JasperReport report = JasperCompileManager.compileReport("relatorios/RelatorioClientes.jrxml");

	//JasperPrint print = JasperFillManager.fillReport(report, null,new JRResultSetDataSource(locacao));

	//JasperExportManager.exportReportToPdfFile(print, "relatorios/RelatorioClientes.pdf");
		
	}
	
	
	
	public void buscaCliente(){
		Cliente c=new Cliente();
		c.setCpf(txtCpfCliente.getText());
		ClienteDao cd=new ClienteDao();
				
			try{
				txtNomeCliente.setText(cd.consultaCliente(c).getNome());
				txtNomeCliente.setEditable(false);
				txtCpfCliente.setEditable(false);
			}
			catch(NullPointerException erro){
				JOptionPane.showMessageDialog(null,"Cliente n�o est� cadastrado no sistema","Erro",JOptionPane.ERROR_MESSAGE);
				txtNomeCliente.setText("");
				txtNomeCliente.setEditable(true);
			}
			catch (SQLException e) {
				JOptionPane.showMessageDialog(null, e.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
			}
			
			
	}
	
	
	public List<Automoveis> listaAutomovel(){
		AutomoveisDao autoDao=new AutomoveisDao();
		List<Automoveis> lista=new ArrayList<Automoveis>();
		lista=autoDao.consultaListaAutomoveis();
		return lista;
	}
	
	
	
	public void preencheComboAutomovel(){
		if(cmbCodigoAutomovel.getItemCount()!=0){
			cmbCodigoAutomovel.removeAllItems();
		}
		List<Automoveis> lista=listaAutomovel();
			if(lista!=null){
				for(Automoveis p:lista){
					cmbCodigoAutomovel.addItem(p.getCodAutomovel()+"-"+p.getMarca()+"  "+p.getModelo()+"  "+p.getCor()+"  "+p.getAno()+"  "+p.getMotor()+"  "+p.getTipo().getTipo());
					
				}
			}
		
	}
	
	public void perfilLocacao(){
		txtNomeCliente.setEditable(false);
		txtCpfCliente.setEditable(false);
		txtDataLocacao.setEditable(false);
		txtDataDevolucao.setEditable(false);
		cmbCodigoAutomovel.setEnabled(false);
		btnGerarRelatorio.setEnabled(true);
		btnCadastrarSimulacao.setEnabled(true);
		
	}
	
	public void perfilCadastraLocacao(){
		txtNomeCliente.setEditable(false);
		txtCpfCliente.setEditable(true);
		txtDataLocacao.setEditable(true);
		txtDataDevolucao.setEditable(true);
		cmbCodigoAutomovel.setEnabled(true);
		txtNomeCliente.setText("");
		txtCpfCliente.setText("");
		txtDataLocacao.setText("");
		txtDataDevolucao.setText("");
		txtValorTotal.setText("");
		cmbCodigoAutomovel.setSelectedIndex(0);
		btnGerarRelatorio.setEnabled(false);
		btnCadastrarSimulacao.setEnabled(false);
		
	}
}
