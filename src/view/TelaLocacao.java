package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.KeyboardFocusManager;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.text.MaskFormatter;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;

import dao.*;
import pojo.*;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;

public class TelaLocacao extends JFrame {

	private JPanel contentPane;
	private JTextField txtValorTotal;
	private JComboBox cmbCodigoAutomovel;
	private JFormattedTextField txtDataDevolucao;
	private JFormattedTextField txtDataLocacao;
	private JRadioButton rdPesquisar;
	private JRadioButton rdAtualizar;
	private JRadioButton rdExcluir;
	private JRadioButton rdCadastrar;
	private JButton btnInserir;
	private JButton btnEditar;
	private JButton btnApagar;
	private JButton btnPesquisar;
	private JTable tabelaLocacao;
	private DefaultTableModel modelo;
	private JComboBox cmbCliente;
	
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaLocacao frame = new TelaLocacao();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws ParseException 
	 */
	public TelaLocacao() throws ParseException {
		setResizable(false);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent arg0) {
				preencheComboAutomovel();
				preencheComboCliente();
				atualizaTabela();
			}
		});
		setTitle("Locacao de Automoveis");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 1054, 645);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblCodigoAutomovel = new JLabel("Seleciona Autom\u00F3vel");
		lblCodigoAutomovel.setHorizontalAlignment(SwingConstants.CENTER);
		lblCodigoAutomovel.setBounds(10, 36, 131, 14);
		contentPane.add(lblCodigoAutomovel);
		
		JLabel lblNewLabel = new JLabel("Seleciona Cliente");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(10, 87, 97, 14);
		contentPane.add(lblNewLabel);
		
		cmbCodigoAutomovel = new JComboBox();
		cmbCodigoAutomovel.setEnabled(false);
		cmbCodigoAutomovel.setBounds(151, 33, 366, 20);
		contentPane.add(cmbCodigoAutomovel);
		
		MaskFormatter data=new MaskFormatter("##/##/####");
		JLabel lblDataLocacao = new JLabel("Data Loca\u00E7\u00E3o");
		lblDataLocacao.setHorizontalAlignment(SwingConstants.CENTER);
		lblDataLocacao.setBounds(10, 134, 101, 14);
		contentPane.add(lblDataLocacao);
		
		txtDataLocacao = new JFormattedTextField(data);
		txtDataLocacao.setEditable(false);
		txtDataLocacao.setBounds(124, 131, 138, 20);
		contentPane.add(txtDataLocacao);
		
		
		txtDataLocacao.setFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS, Collections.EMPTY_SET);
		txtDataLocacao.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent a) {
				if(a.getKeyCode()==KeyEvent.VK_ENTER||a.getKeyCode()==KeyEvent.VK_TAB){
					
				}
			}
		});
		
		JLabel lblDataDevolucao = new JLabel("Data Devolu\u00E7\u00E3o");
		lblDataDevolucao.setBounds(26, 177, 86, 14);
		contentPane.add(lblDataDevolucao);
		
		txtDataDevolucao = new JFormattedTextField(data);
		txtDataDevolucao.setEditable(false);
		txtDataDevolucao.setBounds(124, 174, 138, 20);
		contentPane.add(txtDataDevolucao);
		
		txtDataDevolucao.setFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS, Collections.EMPTY_SET);
		txtDataDevolucao.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent a) {
				if(a.getKeyCode()==KeyEvent.VK_ENTER||a.getKeyCode()==KeyEvent.VK_TAB){
					gerarValorTotal();
				}
			}
		});
		
		
		JLabel lblValorTotal = new JLabel("Valor Total");
		lblValorTotal.setHorizontalAlignment(SwingConstants.CENTER);
		lblValorTotal.setBounds(10, 219, 77, 14);
		contentPane.add(lblValorTotal);
		
		txtValorTotal = new JTextField();
		txtValorTotal.setEditable(false);
		txtValorTotal.setBounds(108, 216, 109, 20);
		contentPane.add(txtValorTotal);
		txtValorTotal.setColumns(10);
		
		MaskFormatter cpf=new MaskFormatter("###.###.###-##");
		
		
		rdCadastrar = new JRadioButton("Adicionar");
		rdCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				perfilCadastrar();
			}
		});
		rdCadastrar.setBounds(591, 32, 109, 23);
		contentPane.add(rdCadastrar);
		
		rdExcluir = new JRadioButton("Remover");
		rdExcluir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				perfilExcluir();
			}
		});
		rdExcluir.setBounds(591, 68, 109, 23);
		contentPane.add(rdExcluir);
		
		rdAtualizar = new JRadioButton("Editar");
		rdAtualizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				perfilAtualizar();
			}
		});
		rdAtualizar.setBounds(591, 109, 109, 23);
		contentPane.add(rdAtualizar);
		
		rdPesquisar = new JRadioButton("Pesquisar");
		rdPesquisar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				perfilPesquisa();
			}
		});
		rdPesquisar.setBounds(591, 152, 109, 23);
		contentPane.add(rdPesquisar);
		
		ButtonGroup grupo=new ButtonGroup();
		grupo.add(rdAtualizar);
		grupo.add(rdPesquisar);
		grupo.add(rdCadastrar);
		grupo.add(rdExcluir);
		
		btnInserir = new JButton("Inserir");
		btnInserir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				cadastraLocacao();
			}
		});
		btnInserir.setVisible(false);
		btnInserir.setBounds(26, 279, 89, 23);
		contentPane.add(btnInserir);
		
		btnEditar = new JButton("Editar");
		btnEditar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				atualizaLocacao();
			}
		});
		btnEditar.setVisible(false);
		btnEditar.setBounds(149, 279, 89, 23);
		contentPane.add(btnEditar);
		
		btnApagar = new JButton("Apagar");
		btnApagar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ExcluiLocacao();
			}
		});
		btnApagar.setVisible(false);
		btnApagar.setBounds(268, 279, 89, 23);
		contentPane.add(btnApagar);
		
		btnPesquisar = new JButton("Pesquisar");
		btnPesquisar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				pesquisaLocacao();
			}
		});
		btnPesquisar.setVisible(false);
		btnPesquisar.setVisible(false);
		btnPesquisar.setBounds(381, 279, 89, 23);
		contentPane.add(btnPesquisar);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(40, 333, 935, 249);
		contentPane.add(scrollPane);
		
		tabelaLocacao = new JTable();
		modelo=new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Cod Auto", "Marca", "Modelo", "Cor", "ano", "motor", "Tipo", "CPF", "Nome", "Data Loca\u00E7\u00E3o", "Data Devolu\u00E7\u00E3o", "Valor Total"
			}
		);
		tabelaLocacao.setModel(modelo);
		tabelaLocacao.getColumnModel().getColumn(9).setPreferredWidth(87);
		tabelaLocacao.getColumnModel().getColumn(10).setPreferredWidth(104);
		scrollPane.setViewportView(tabelaLocacao);
		
		cmbCliente = new JComboBox();
		cmbCliente.setEnabled(false);
		cmbCliente.setBounds(124, 84, 397, 20);
		contentPane.add(cmbCliente);
		
	}
	
	
	public Locacao getLocacao(){
		Locacao loc=new Locacao();
		Cliente c=new Cliente();
		Automoveis auto=new Automoveis();
		
		String texto2=cmbCliente.getSelectedItem().toString();
		String codigoAuto2=texto2.substring(0,texto2.lastIndexOf("  ")) ;
		
		c.setCpf(codigoAuto2);
		//c.setNome(txtNomeCliente.getText());
		
		String texto=cmbCodigoAutomovel.getSelectedItem().toString();
		String codigoAuto=texto.substring(0,texto.lastIndexOf("-"));
		System.out.println(codigoAuto);
		auto.setCodAutomovel(Integer.parseInt(codigoAuto));
		
		loc.setAutomovel(auto);
		loc.setCliente(c);
		loc.setDataLocacao(txtDataLocacao.getText());
		loc.setDataDevolucao(txtDataDevolucao.getText());
		
		return loc;
		
	}
	
	public void setLocacao(Locacao locacao){
		cmbCliente.setSelectedItem(locacao.getCliente().getCpf()+" "+locacao.getCliente().getNome()+" "+locacao.getCliente().getIdade());
		
		
		txtDataLocacao.setText(locacao.getDataLocacao());
		txtDataDevolucao.setText(locacao.getDataDevolucao());
		txtValorTotal.setText(String.valueOf(locacao.getValorTotal()));
	/*	String result=String.valueOf(locacao.getAutomovel().getCodAutomovel()-cmbCodigoAutomovel.getItemCount()); 
		String temp=String.valueOf(result.charAt(result.length()-1));*/
		cmbCodigoAutomovel.setSelectedItem(locacao.getAutomovel().getCodAutomovel()+"-"+locacao.getAutomovel().getMarca()+"  "+locacao.getAutomovel().getModelo()+"  "+locacao.getAutomovel().getCor()+"  "+locacao.getAutomovel().getAno()+"  "+locacao.getAutomovel().getMotor()+"  "+locacao.getAutomovel().getTipo().getTipo());
		
	}
	
	
	public List<Automoveis> listaAutomovel(){
		AutomoveisDao autoDao=new AutomoveisDao();
		List<Automoveis> lista=new ArrayList<Automoveis>();
		lista=autoDao.consultaListaAutomoveis();
		return lista;
	}
	
	
	
	public void preencheComboAutomovel(){
		if(cmbCodigoAutomovel.getItemCount()!=0){
			cmbCodigoAutomovel.removeAllItems();
		}
		List<Automoveis> lista=listaAutomovel();
			if(lista!=null){
				for(Automoveis p:lista){
					cmbCodigoAutomovel.addItem(p.getCodAutomovel()+"-"+p.getMarca()+"  "+p.getModelo()+"  "+p.getCor()+"  "+p.getAno()+"  "+p.getMotor()+"  "+p.getTipo().getTipo());
					
				}
			}
		
	}
	
	public List<Cliente> listaCliente(){
		ClienteDao cliDao=new ClienteDao();
		List<Cliente> lista=new ArrayList<Cliente>();
		lista=cliDao.consultaListaCliente();
		return lista;
	}
	
	
	
	public void preencheComboCliente(){
		if(cmbCliente.getItemCount()!=0){
			cmbCliente.removeAllItems();
		}
		List<Cliente> lista=listaCliente();
			if(lista!=null){
				for(Cliente p:lista){
					cmbCliente.addItem(p.getCpf()+"  "+p.getNome()+" "+p.getIdade());
					
				}
			}
		
	}
	
	
	/*public void buscaCliente(){
		Cliente c=new Cliente();
		c.setCpf(txtCpfCliente.getText());
		ClienteDao cd=new ClienteDao();
				
			try{
				txtNomeCliente.setText(cd.consultaCliente(c).getNome());
			}
			catch(NullPointerException erro){
				JOptionPane.showMessageDialog(null,"Para fazer a loca��o � necess�rio que o cliente esteja cadastrado no sistema","Erro",JOptionPane.ERROR_MESSAGE);
			}
			catch (SQLException e) {
				JOptionPane.showMessageDialog(null, e.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
			}
			
			
	}*/
	
	public void perfilCadastrar(){
		cmbCliente.setEnabled(true);
		txtDataDevolucao.setEditable(true);
		txtDataLocacao.setEditable(true);
		cmbCodigoAutomovel.setEnabled(true);
		btnInserir.setVisible(true);
		btnEditar.setVisible(false);
		btnApagar.setVisible(false);
		btnPesquisar.setVisible(false);
	
	}
	
	public void perfilAtualizar(){
		cmbCliente.setEnabled(false);
		txtDataDevolucao.setEditable(true);
		txtDataLocacao.setEditable(false);
		cmbCodigoAutomovel.setEnabled(false);
		btnInserir.setVisible(false);
		btnEditar.setVisible(true);
		btnApagar.setVisible(false);
		btnPesquisar.setVisible(false);
	
	}
	
	public void perfilExcluir(){
		cmbCliente.setEnabled(false);
		txtDataDevolucao.setEditable(false);
		txtDataLocacao.setEditable(false);
		cmbCodigoAutomovel.setEnabled(false);
		btnInserir.setVisible(false);
		btnEditar.setVisible(false);
		btnApagar.setVisible(true);
		btnPesquisar.setVisible(false);
	
	}
	
	public void perfilPadrao(){
		cmbCliente.setEnabled(false);
		txtDataDevolucao.setEditable(false);
		txtDataLocacao.setEditable(false);
		cmbCodigoAutomovel.setEnabled(false);
		btnInserir.setVisible(false);
		btnEditar.setVisible(false);
		btnApagar.setVisible(false);
		btnPesquisar.setVisible(false);
		cmbCliente.setSelectedIndex(0);
		txtDataDevolucao.setText("");
		txtDataLocacao.setText("");
		txtValorTotal.setText("");
		cmbCodigoAutomovel.setSelectedIndex(0);
		
	}
	
	public void perfilPesquisa(){
		cmbCliente.setEnabled(true);
		txtDataDevolucao.setEditable(false);
		txtDataLocacao.setEditable(true);
		cmbCodigoAutomovel.setEnabled(true);
		btnInserir.setVisible(false);
		btnEditar.setVisible(false);
		btnApagar.setVisible(false);
		btnPesquisar.setVisible(true);
	}
	
	
	public void gerarValorTotal(){
		LocacaoDao dl=new LocacaoDao();
		Locacao loc=new Locacao();
		
		txtValorTotal.setText(String.valueOf((dl.consultaValorTotal(getLocacao()).getValorTotal())));
		
	}
	
	public void cadastraLocacao(){
		
		
		try {
			LocacaoDao locDao=new LocacaoDao();
			Locacao loc=getLocacao();
			locDao.cadastraLocacao(loc);
			atualizaTabela();
			perfilPadrao();
			
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, e.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
		catch(NumberFormatException e){
			JOptionPane.showMessageDialog(null, "Dados digitados incorretamente ou campos em branco");
			e.printStackTrace();
		}
	}
	
	
	public void ExcluiLocacao(){
		LocacaoDao locDao=new LocacaoDao();
		Cliente c=new Cliente();
		Locacao loc=new Locacao();
		Automoveis auto=new Automoveis();
			
		try{
			String texto=cmbCliente.getSelectedItem().toString();
			String codigoAuto=texto.substring(0,texto.lastIndexOf("  "));
			System.out.println(codigoAuto);
			c.setCpf(codigoAuto);
			
			String texto2=cmbCodigoAutomovel.getSelectedItem().toString();
			String codigoAuto2=texto2.substring(0,texto2.lastIndexOf("-"));
			auto.setCodAutomovel(Integer.parseInt(codigoAuto2));
			
			loc.setCliente(c);
			loc.setAutomovel(auto);
			loc.setDataLocacao(txtDataLocacao.getText());
			locDao.excluiLocacao(loc);
			atualizaTabela();
			perfilPadrao();
		}
		catch(SQLException e){
			JOptionPane.showMessageDialog(null, e.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
		catch(NumberFormatException e){
			JOptionPane.showMessageDialog(null, "Dados digitados incorretos ou campos em branco");
		}
	}
	
	
	public void atualizaLocacao(){
		
			
		try{
			LocacaoDao locDao=new LocacaoDao();
			Locacao loc=getLocacao();
			locDao.atualizaLocacao(loc);
			atualizaTabela();
			perfilPadrao();
		}
		catch(SQLException e){
			JOptionPane.showMessageDialog(null, e.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
		catch(NumberFormatException e){
			JOptionPane.showMessageDialog(null, "Dados digitados incorretos ou campos em branco");
		}
	}
	
	
	public void pesquisaLocacao(){
		LocacaoDao locDao=new LocacaoDao();
		Cliente c=new Cliente();
		Locacao loc=new Locacao();
		Automoveis auto=new Automoveis();
		
		try{
		String texto=cmbCliente.getSelectedItem().toString();
		String codigoAuto=texto.substring(0,texto.lastIndexOf("  "));	
		c.setCpf(codigoAuto);
		String texto2=cmbCodigoAutomovel.getSelectedItem().toString();
		String codigoAuto2=texto2.substring(0,texto2.lastIndexOf("-"));
		auto.setCodAutomovel(Integer.parseInt(codigoAuto2));
		loc.setCliente(c);
		loc.setAutomovel(auto);
		loc.setDataLocacao(txtDataLocacao.getText());
		
		setLocacao(locDao.consultaLocacao(loc));
		}
		catch(SQLException e){
			JOptionPane.showMessageDialog(null, e.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
		}
	}
	
	public List<Locacao> listaLocacao(){
		LocacaoDao locDao=new LocacaoDao();
		List<Locacao> lista=new ArrayList<Locacao>();
		lista=locDao.consultaListaLocacao();
		return lista;
	}
	
	
	public void atualizaTabela(){
		List<Locacao> lista=listaLocacao();
		
		if(modelo.getRowCount()!=0){
			modelo.setRowCount(0);
		}
		if(lista!=null){
			for(Locacao loc:lista){
				Object[] objeto=new Object[12];
				objeto[0]=loc.getAutomovel().getCodAutomovel();
				objeto[1]=loc.getAutomovel().getMarca();
				objeto[2]=loc.getAutomovel().getModelo();
				objeto[3]=loc.getAutomovel().getCor();
				objeto[4]=loc.getAutomovel().getAno();
				objeto[5]=loc.getAutomovel().getMotor();
				objeto[6]=loc.getAutomovel().getTipo().getTipo();
				objeto[7]=loc.getCliente().getCpf();
				objeto[8]=loc.getCliente().getNome();
				objeto[9]=loc.getDataLocacao();
				objeto[10]=loc.getDataDevolucao();
				objeto[11]=loc.getValorTotal();
				modelo.addRow(objeto);
				
			}
		}
		
	}
	
	
}
