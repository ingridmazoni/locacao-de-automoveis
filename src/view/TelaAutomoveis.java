package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JRadioButton;
import javax.swing.JButton;
import pojo.*;
import dao.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.Color;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;

public class TelaAutomoveis extends JFrame {
	private JPanel contentPane;
	private JTextField txtCodigoAutomovel;
	private JTextField txtmarca;
	private JTextField txtmodelo;
	private JTextField txtcor;
	private JTextField txtmotor;
	private JTextField txtano;
	private JComboBox cbTipo;
	private JRadioButton rdPesquisar;
	private JRadioButton rdAtualizar;
	private JRadioButton rdExcluir;
	private JRadioButton rdCadastrar;
	private JButton btnInserir;
	private JButton btnApagar;
	private JButton btnEditar;
	private JTable tabelaAutomovel;
	private DefaultTableModel modelo;
	
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaAutomoveis frame = new TelaAutomoveis();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TelaAutomoveis() {
		setResizable(false);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent arg0) {
				perfilPadrao();
				preencheComboTipo();
				atualizaTabela();
				
			}
		});
		setTitle("Cadastro de Autom\u00F3veis");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 631, 650);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblCdigoAutomvel = new JLabel("C\u00F3digo Autom\u00F3vel");
		lblCdigoAutomvel.setBounds(26, 37, 101, 14);
		contentPane.add(lblCdigoAutomvel);
		
		txtCodigoAutomovel = new JTextField();
		txtCodigoAutomovel.setEditable(false);
		txtCodigoAutomovel.setBounds(150, 34, 153, 20);
		contentPane.add(txtCodigoAutomovel);
		txtCodigoAutomovel.setColumns(10);
		
		JLabel lblMarca = new JLabel("Marca");
		lblMarca.setBounds(26, 72, 46, 14);
		contentPane.add(lblMarca);
		
		txtmarca = new JTextField();
		txtmarca.setEditable(false);
		txtmarca.setBounds(82, 69, 170, 20);
		contentPane.add(txtmarca);
		txtmarca.setColumns(10);
		
		JLabel lblModelo = new JLabel("Modelo");
		lblModelo.setBounds(26, 115, 46, 14);
		contentPane.add(lblModelo);
		
		txtmodelo = new JTextField();
		txtmodelo.setEditable(false);
		txtmodelo.setBounds(82, 112, 180, 20);
		contentPane.add(txtmodelo);
		txtmodelo.setColumns(10);
		
		JLabel lblCor = new JLabel("Cor");
		lblCor.setBounds(26, 156, 46, 14);
		contentPane.add(lblCor);
		
		txtcor = new JTextField();
		txtcor.setEditable(false);
		txtcor.setBounds(82, 153, 180, 20);
		contentPane.add(txtcor);
		txtcor.setColumns(10);
		
		JLabel lblMotor = new JLabel("Motor");
		lblMotor.setBounds(26, 198, 46, 14);
		contentPane.add(lblMotor);
		
		txtmotor = new JTextField();
		txtmotor.setEditable(false);
		txtmotor.setBounds(82, 195, 208, 20);
		contentPane.add(txtmotor);
		txtmotor.setColumns(10);
		
		JLabel lblAno = new JLabel("Ano");
		lblAno.setBounds(272, 115, 46, 14);
		contentPane.add(lblAno);
		
		txtano = new JTextField();
		txtano.setEditable(false);
		txtano.setBounds(331, 112, 86, 20);
		contentPane.add(txtano);
		txtano.setColumns(10);
		
		cbTipo = new JComboBox();
		cbTipo.setEnabled(false);
		cbTipo.setModel(new DefaultComboBoxModel(new String[] {""}));
		cbTipo.setBounds(87, 237, 223, 20);
		cbTipo.setMaximumRowCount(9);
		cbTipo.setForeground(new Color(0, 0, 0));
		contentPane.add(cbTipo);
		
		JLabel lblTipo = new JLabel("Tipo");
		lblTipo.setBounds(26, 240, 46, 14);
		contentPane.add(lblTipo);
		
		rdCadastrar = new JRadioButton("Adicionar");
		rdCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				limpar();
				perfilCadastrar();
			}

			
		});
		rdCadastrar.setBounds(492, 33, 109, 23);
		contentPane.add(rdCadastrar);
		
		rdExcluir = new JRadioButton("Remover");
		rdExcluir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				perfilExcluirPesquisar();
			}
		});
		rdExcluir.setBounds(492, 68, 109, 23);
		contentPane.add(rdExcluir);
		
		rdAtualizar = new JRadioButton("Editar");
		rdAtualizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				perfilAtualizar();
			}
		});
		rdAtualizar.setBounds(492, 111, 109, 23);
		contentPane.add(rdAtualizar);
		
		rdPesquisar = new JRadioButton("Pesquisar");
		rdPesquisar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				perfilExcluirPesquisar();
				pesquisar();
			}
		});
		rdPesquisar.setBounds(492, 152, 109, 23);
		contentPane.add(rdPesquisar);
		
		btnInserir = new JButton("Inserir");
		btnInserir.setVisible(false);
		btnInserir.setBounds(55, 306, 89, 23);
		btnInserir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				adiciona();
			}
		});
		contentPane.add(btnInserir);
		
		
		
		ButtonGroup grupo=new ButtonGroup();
		grupo.add(rdAtualizar);
		grupo.add(rdPesquisar);
		grupo.add(rdCadastrar);
		grupo.add(rdExcluir);
		
		btnEditar = new JButton("Editar ");
		btnEditar.setVisible(false);
		btnEditar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				atualizar();
			}
		});
		btnEditar.setBounds(163, 306, 89, 23);
		contentPane.add(btnEditar);
		
		btnApagar = new JButton("Apagar");
		btnApagar.setVisible(false);
		btnApagar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				exclui();
			}
		});
		btnApagar.setBounds(272, 306, 89, 23);
		contentPane.add(btnApagar);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(55, 370, 546, 213);
		contentPane.add(scrollPane);
		
		tabelaAutomovel = new JTable();
		
		modelo=new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"C�digo", "Marca", "Modelo", "Cor", "Ano", "Motor", "Tipo"
			}
		);
		tabelaAutomovel.setModel(modelo);
		scrollPane.setViewportView(tabelaAutomovel);
		
		
		
	
		
	}
	
	
	public void setAutomoveis(Automoveis automovel)throws NumberFormatException{
		
			txtCodigoAutomovel.setText(String.valueOf(automovel.getCodAutomovel()));
			txtmarca.setText(automovel.getMarca());
			txtmodelo.setText(automovel.getModelo());
			txtcor.setText(automovel.getCor());
			txtano.setText(String.valueOf(automovel.getAno()));
			txtmotor.setText(String.valueOf(automovel.getMotor()));
			cbTipo.setSelectedItem(automovel.getTipo().getCodTipo()+"-"+automovel.getTipo().getTipo());
			
	
		
	}
	
	
	
	public Automoveis getAutomoveis()throws NumberFormatException{
		
		Automoveis auto=new Automoveis();
		Tipo tipo=new Tipo();
		
	
			auto.setMarca(txtmarca.getText());
			auto.setModelo(txtmodelo.getText());
			auto.setCor(txtcor.getText());
			auto.setAno(Integer.parseInt(txtano.getText()));
			auto.setMotor(Double.parseDouble(txtmotor.getText()));
			
			String texto=cbTipo.getSelectedItem().toString();
			String codigoTipo=texto.substring(0,texto.lastIndexOf("-"));
			
			tipo.setCodTipo(Integer.parseInt(codigoTipo));
			auto.setTipo(tipo);
				
		return auto;
				
	}
	
	public void adiciona() {
		AutomoveisDao autoDao=new AutomoveisDao();
		Automoveis auto;
		
			try{
				auto=getAutomoveis();
				autoDao.insereAutomoveis(auto);
				atualizaTabela();
				JOptionPane.showMessageDialog(null, "Automovel Inserido com sucesso");
				perfilPadrao();
			}
			catch(SQLException e){
				JOptionPane.showMessageDialog(null, e.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
			}
			catch(NumberFormatException e){
				JOptionPane.showMessageDialog(null,"Os dados foram digitados incorretamante","Erro",JOptionPane.ERROR_MESSAGE);
				e.printStackTrace();
				
			}
	}
	
	public void exclui(){
		AutomoveisDao autoDao=new AutomoveisDao();
				
			try{
				Automoveis auto=new Automoveis();
				auto.setCodAutomovel(Integer.parseInt(txtCodigoAutomovel.getText()));
				autoDao.excluiAutomoveis(auto);
				atualizaTabela();
				JOptionPane.showMessageDialog(null, "Automovel Excluido com sucesso");
				perfilPadrao();
			}
			catch(SQLException e){
				JOptionPane.showMessageDialog(null, e.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
				
			}
			catch(NumberFormatException e){
				JOptionPane.showMessageDialog(null,"Os dados foram digitados incorretamante","Erro",JOptionPane.ERROR_MESSAGE);
				e.printStackTrace();
				
			}
			
	}
		
	public void atualizar(){
		AutomoveisDao autoDao=new AutomoveisDao();
					
		try{
			Automoveis auto=getAutomoveis();
			auto.setCodAutomovel(Integer.parseInt(txtCodigoAutomovel.getText()));
			autoDao.atualizaAutomoveis(auto);
			atualizaTabela();
			JOptionPane.showMessageDialog(null, "Automovel Atualizado com sucesso");
			perfilPadrao();
		}
		catch(SQLException e){
			JOptionPane.showMessageDialog(null, e.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
		}
		catch(NumberFormatException e){
			JOptionPane.showMessageDialog(null,"Os dados foram digitados incorretamante","Erro",JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
			
		}
			
	}
	
	
	public void pesquisar(){
		AutomoveisDao autoDao=new AutomoveisDao();
		Automoveis auto=new Automoveis();
		
		try {
			
			auto.setCodAutomovel(Integer.parseInt(JOptionPane.showInputDialog("Digite o c�digo do autom�vel para fazer a busca")));
			setAutomoveis(autoDao.consultaAutomoveis(auto));
			
		} catch (NullPointerException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null,"O automovel n�o foi localizado","Erro",JOptionPane.ERROR_MESSAGE);
			
		}
		catch(NumberFormatException e){
			e.printStackTrace();
			JOptionPane.showMessageDialog(null,"Para fazer a pesquisa digite apenas numeros","Erro",JOptionPane.ERROR_MESSAGE);
		}
		catch (SQLException e) {
			JOptionPane.showMessageDialog(null, e.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
		
		
	}
	
	
	
	public List<Tipo> listaTipo(){
		TipoDao tipoDao=new TipoDao();
		List<Tipo> lista=new ArrayList<Tipo>();
		lista=tipoDao.consultaListaTipo();
		return lista;
	}
	
	
	
	public void preencheComboTipo(){
		if(cbTipo.getItemCount()!=0){
			cbTipo.removeAllItems();
		}
		List<Tipo> lista=listaTipo();
			if(lista!=null){
				for(Tipo p:lista){
					cbTipo.addItem(p.getCodTipo()+"-"+p.getTipo());
					
				}
			}
		
	}
	
	public List<Automoveis> listaAutomoveis(){
		AutomoveisDao autoDao=new AutomoveisDao();
		List<Automoveis> lista=new ArrayList<Automoveis>();
		lista=autoDao.consultaListaAutomoveis();
		return lista;
	}
	
	
	public void atualizaTabela(){
		List<Automoveis> lista=listaAutomoveis();
		
		if(modelo.getRowCount()!=0){
			modelo.setRowCount(0);
		}
		if(lista!=null){
			for(Automoveis p:lista){
				Object[] objeto=new Object[7];
				objeto[0]=p.getCodAutomovel();
				objeto[1]=p.getMarca();
				objeto[2]=p.getModelo();
				objeto[3]=p.getCor();
				objeto[4]=p.getAno();
				objeto[5]=p.getMotor();
				objeto[6]=p.getTipo().getTipo();
				modelo.addRow(objeto);
				
			}
		}
		
	}
	
	
	public void perfilExcluirPesquisar(){
		txtCodigoAutomovel.setEditable(false);
		txtmarca.setEditable(false);
		txtmodelo.setEditable(false);
		txtcor.setEditable(false);
		txtano.setEditable(false);
		txtmotor.setEditable(false);
		cbTipo.setEnabled(false);
		btnInserir.setVisible(false);
		btnApagar.setVisible(true);
		btnEditar.setVisible(false);
		
		
	}
	
	public void perfilAtualizar(){
		txtCodigoAutomovel.setEditable(false);
		txtmarca.setEditable(true);
		txtmodelo.setEditable(true);
		txtcor.setEditable(true);
		txtano.setEditable(true);
		txtmotor.setEditable(true);
		cbTipo.setEnabled(true);
		btnInserir.setVisible(false);
		btnApagar.setVisible(false);
		btnEditar.setVisible(true);
		
		
	}
	
	public void perfilCadastrar(){
		txtCodigoAutomovel.setEditable(false);
		txtmarca.setEditable(true);
		txtmodelo.setEditable(true);
		txtcor.setEditable(true);
		txtano.setEditable(true);
		txtmotor.setEditable(true);
		cbTipo.setEnabled(true);
		btnInserir.setVisible(true);
		btnApagar.setVisible(false);
		btnEditar.setVisible(false);
		
		
	}

	public void perfilPadrao(){
		txtCodigoAutomovel.setEditable(false);
		txtmarca.setEditable(false);
		txtmodelo.setEditable(false);
		txtcor.setEditable(false);
		txtano.setEditable(false);
		txtmotor.setEditable(false);
		cbTipo.setEnabled(false);
		btnInserir.setVisible(false);
		btnApagar.setVisible(false);
		btnEditar.setVisible(false);
		limpar();
		
		
	}
	
	
	public void limpar(){
		txtCodigoAutomovel.setText("");
		txtmarca.setText("");
		txtmodelo.setText("");
		txtcor.setText("");
		txtano.setText("");
		txtmotor.setText("");
		cbTipo.setSelectedIndex(0);
	}
	}
	

