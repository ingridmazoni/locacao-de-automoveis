package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.ParseException;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Menu extends JFrame {

	private JPanel contentPane;
	private TelaAutomoveis telaAuto;
	private TelaCliente telaCli;
	private TelaLocacao telaLoc;
	private TelaSimulacao telaSimu;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Menu frame = new Menu();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Menu() {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent arg0) {
				telaAuto=new TelaAutomoveis();
				try {
					telaCli=new TelaCliente();
					telaLoc=new TelaLocacao();
					telaSimu=new TelaSimulacao();
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnNewMenu = new JMenu("Cadastrar");
		menuBar.add(mnNewMenu);
		
		JMenuItem mntmAutomoveis = new JMenuItem("Automoveis");
		mntmAutomoveis.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				telaAuto.show();
			}
		});
		mnNewMenu.add(mntmAutomoveis);
		
		JMenuItem mntmClientes = new JMenuItem("Clientes");
		mntmClientes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				telaCli.show();
			}
		});
		mnNewMenu.add(mntmClientes);
		
		JMenuItem mntmLocao = new JMenuItem("Loca\u00E7\u00E3o");
		mntmLocao.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				telaLoc.show();
			}
		});
		mnNewMenu.add(mntmLocao);
		
		JMenu mnSimular = new JMenu("Simular");
		menuBar.add(mnSimular);
		
		JMenuItem mntmLocao_1 = new JMenuItem("Loca\u00E7\u00E3o");
		mntmLocao_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				telaSimu.show();
			}
		});
		mnSimular.add(mntmLocao_1);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
	}
}
