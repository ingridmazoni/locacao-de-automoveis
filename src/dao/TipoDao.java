package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import pojo.Tipo;

public class TipoDao {
	
Connection c;
	
	public TipoDao(){
		GenericDao gDao=new GenericDao();
		c=gDao.getConnection();
	}
	
	

	public List<Tipo> consultaListaTipo(){
		List<Tipo> listaTipo=new ArrayList<Tipo>();
		StringBuffer sql=new StringBuffer();
		sql.append("select * from tipo");
		
		
		 try {
			PreparedStatement ps=c.prepareStatement(sql.toString());
			ResultSet rs=ps.executeQuery();
			
			while(rs.next()){
				Tipo TipoConsultado=new Tipo();
				TipoConsultado.setCodTipo(rs.getInt("codTipo"));
				TipoConsultado.setTipo(rs.getString("tipo"));
				listaTipo.add(TipoConsultado);
			}
			
			rs.close();
			ps.close();
			
			
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, e.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
		}
			
		 return listaTipo;
		 
	}
	
	
	
}
