package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import pojo.*;

public class AutomoveisDao {
Connection c;
	
	public AutomoveisDao(){
		GenericDao gDao=new GenericDao();
		c=gDao.getConnection();
	}
	
	public boolean insereAutomoveis(Automoveis automoveis)throws SQLException{
		boolean inserido=false;
		String sql="INSERT INTO automoveis VALUES(?,?,?,?,?,?)";
		
			PreparedStatement ps=c.prepareStatement(sql);
			ps.setString(1, automoveis.getMarca());
			ps.setString(2, automoveis.getModelo());
			ps.setString(3, automoveis.getCor());
			ps.setInt(4, automoveis.getAno());
			ps.setDouble(5, automoveis.getMotor());
			ps.setInt(6, automoveis.getTipo().getCodTipo());
			ps.execute();
			ps.close();
			inserido=true;
		
		
		return inserido;
		
	}
	
	public boolean atualizaAutomoveis(Automoveis automoveis)throws SQLException{
		boolean atualizado=false;
		String sql="UPDATE automoveis set marca=?,modelo=?,cor=?,ano=?,motor=?,codTipo=? where codAutomovel=?";
		
			PreparedStatement ps=c.prepareStatement(sql);
			ps.setString(1, automoveis.getMarca());
			ps.setString(2, automoveis.getModelo());
			ps.setString(3, automoveis.getCor());
			ps.setInt(4, automoveis.getAno());
			ps.setDouble(5, automoveis.getMotor());
			ps.setInt(6, automoveis.getTipo().getCodTipo());
			ps.setInt(7, automoveis.getCodAutomovel());
			ps.execute();
			ps.close();
			atualizado=true;
			
		return atualizado;
		
	}
	
	public boolean excluiAutomoveis(Automoveis automoveis)throws SQLException{
		boolean excluido=false;
		String sql="DELETE from automoveis where codAutomovel=?";
		
		PreparedStatement ps;
		
			ps = c.prepareStatement(sql);
			ps.setInt(1, automoveis.getCodAutomovel());
			ps.execute();
			ps.close();
			excluido=true;
			
			
		return excluido;
	}
	
	public Automoveis consultaAutomoveis(Automoveis automoveis)throws SQLException{
		Automoveis AutomoveisConsultado=null;
		Tipo tipo=new Tipo();
		
		StringBuffer sql=new StringBuffer();
		sql.append("select automoveis.codAutomovel,automoveis.marca,automoveis.modelo,automoveis.cor,"+
				"automoveis.ano,automoveis.motor,automoveis.codTipo,tipo.tipo " +
				"from automoveis inner join tipo on automoveis.codTipo=tipo.codTipo where codAutomovel=?");
		
		
			PreparedStatement ps=c.prepareStatement(sql.toString());
			ps.setInt(1, automoveis.getCodAutomovel());
			ResultSet rs=ps.executeQuery();
			
			if(rs.next()){
				AutomoveisConsultado=new Automoveis();
				AutomoveisConsultado.setCodAutomovel(rs.getInt("codAutomovel"));
				AutomoveisConsultado.setMarca(rs.getString("marca"));
				AutomoveisConsultado.setModelo(rs.getString("modelo"));
				AutomoveisConsultado.setCor(rs.getString("cor"));
				AutomoveisConsultado.setAno(rs.getInt("ano"));
				AutomoveisConsultado.setMotor(rs.getDouble("motor"));
				tipo.setCodTipo(rs.getInt("codTipo"));
				tipo.setTipo(rs.getString("tipo"));
				AutomoveisConsultado.setTipo(tipo);
						
			}
			rs.close();
			ps.close();
			
		
		return AutomoveisConsultado;
		
	}
	
	public List<Automoveis> consultaListaAutomoveis(){
		List<Automoveis> listaAutomoveis=new ArrayList<Automoveis>();
		StringBuffer sql=new StringBuffer();
		sql.append("select * from consultaListaAutomoveis");
		
		
		 try {
			PreparedStatement ps=c.prepareStatement(sql.toString());
			ResultSet rs=ps.executeQuery();
			
			while(rs.next()){
				Automoveis AutomoveisConsultado=new Automoveis();
				Tipo tipo=new Tipo();
				
				AutomoveisConsultado.setCodAutomovel(rs.getInt("codAutomovel"));
				AutomoveisConsultado.setMarca(rs.getString("marca"));
				AutomoveisConsultado.setModelo(rs.getString("modelo"));
				AutomoveisConsultado.setCor(rs.getString("cor"));
				AutomoveisConsultado.setAno(rs.getInt("ano"));
				AutomoveisConsultado.setMotor(rs.getDouble("motor"));
				tipo.setCodTipo(rs.getInt("codTipo"));
				tipo.setTipo(rs.getString("tipo"));
				AutomoveisConsultado.setTipo(tipo);
				listaAutomoveis.add(AutomoveisConsultado);
			}
			
			rs.close();
			ps.close();
			
			
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, e.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
		}
			
		 return listaAutomoveis;
		 
		
		
	}
}