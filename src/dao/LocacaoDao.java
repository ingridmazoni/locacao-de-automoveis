package dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import pojo.*;

public class LocacaoDao {
Connection c;
	
	public LocacaoDao(){
		GenericDao gDao=new GenericDao();
		c=gDao.getConnection();
	}
	
	public boolean cadastraLocacao(Locacao locacao)throws SQLException{
		boolean inserido=false;
		String sql="{call cadastraLocacao(?,?,?,?)}";
		
			CallableStatement cs=c.prepareCall(sql);
			cs.setString(1, locacao.getCliente().getCpf());
			cs.setInt(2, locacao.getAutomovel().getCodAutomovel());
			cs.setString(3, locacao.getDataLocacao());
			cs.setString(4, locacao.getDataDevolucao());
			cs.execute();
			cs.close();
			inserido=true;
		return inserido;
		
	}
	public boolean atualizaLocacao(Locacao locacao)throws SQLException{
		boolean atualizado=false;
		
		
			String sql="{call atualizaLocacao(?,?,?,?)}";
			CallableStatement cs=c.prepareCall(sql);
			cs.setString(1, locacao.getCliente().getCpf());
			cs.setInt(2, locacao.getAutomovel().getCodAutomovel());
			cs.setString(3, locacao.getDataLocacao());
			cs.setString(4, locacao.getDataDevolucao());
			cs.execute();
			cs.close();
			
			atualizado=true;
			
			
		return atualizado;
		
	}
	
	public boolean excluiLocacao(Locacao locacao)throws SQLException{
		boolean excluido=false;
		String sql="delete from locacao where cpf=? and codAutomovel=? and dataLocacao=cast(? as date)";
		
		PreparedStatement ps;
	
			ps = c.prepareStatement(sql);
			ps.setString(1,locacao.getCliente().getCpf());
			ps.setInt(2, locacao.getAutomovel().getCodAutomovel());
			ps.setString(3, locacao.getDataLocacao());
			ps.execute();
			ps.close();
			excluido=true;
				
		return excluido;
	}
	
	public Locacao consultaLocacao(Locacao locacao)throws SQLException{
		Locacao LocacaoConsultado=null;
		Automoveis auto=new Automoveis();
		Cliente cli=new Cliente();
				
		
			
			String sql="{call selecionaLocacao(?,?,?)}";
			
			CallableStatement cs=c.prepareCall(sql);
			
			cs.setInt(1, locacao.getAutomovel().getCodAutomovel());
			cs.setString(2,locacao.getCliente().getCpf());
			cs.setString(3,locacao.getDataLocacao());
			ResultSet rs=cs.executeQuery();
			
			if(rs.next()){
				LocacaoConsultado=new Locacao();
				auto.setCodAutomovel(rs.getInt("codAutomovel"));
				cli.setCpf(rs.getString("cpf"));
				cli.setNome(rs.getString("nome"));
				LocacaoConsultado.setAutomovel(auto);
				LocacaoConsultado.setCliente(cli);
				LocacaoConsultado.setDataLocacao(rs.getString("dataLocacao"));
				LocacaoConsultado.setDataDevolucao(rs.getString("dataDevolucao"));
				LocacaoConsultado.setValorTotal(rs.getDouble("valorTotal"));
			}
			rs.close();
			cs.close();
			
			 
			
		
		return LocacaoConsultado;
		
	}
	
	public List<Locacao> consultaListaLocacao(){
		List<Locacao> listaLocacao=new ArrayList<Locacao>();
		Locacao locacao=null;
		StringBuffer sql=new StringBuffer();
		sql.append("select * from selecionaListaLocacao");
		
		
		 try {
			 	locacao=new Locacao();
			 	PreparedStatement ps=c.prepareStatement(sql.toString());
				ResultSet rs=ps.executeQuery();
			
			while(rs.next()){
				Locacao LocacaoConsultado=new Locacao();
				Automoveis auto=new Automoveis();
				Cliente cli=new Cliente();
				Tipo tipo=new Tipo();
				
				auto.setCodAutomovel(rs.getInt("codAutomovel"));
				auto.setMarca(rs.getString("marca"));
				auto.setModelo(rs.getString("modelo"));
				auto.setCor(rs.getString("cor"));
				auto.setAno(rs.getInt("ano"));
				auto.setMotor(rs.getDouble("motor"));
				tipo.setCodTipo(rs.getInt("codTipo"));
				tipo.setTipo(rs.getString("tipo"));
				auto.setTipo(tipo);
				cli.setCpf(rs.getString("cpf"));
				cli.setNome(rs.getString("nome"));
				
				LocacaoConsultado.setAutomovel(auto);
				LocacaoConsultado.setCliente(cli);
				LocacaoConsultado.setDataLocacao(rs.getString("dataLocacao"));
				LocacaoConsultado.setDataDevolucao(rs.getString("dataDevolucao"));
				LocacaoConsultado.setValorTotal(rs.getDouble("valorTotal"));
				listaLocacao.add(LocacaoConsultado);
			}
			
			rs.close();
			rs.close();
			
			
		} catch (SQLException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, e.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
		}
			
		 return listaLocacao;
		 
	}
	
	public Locacao consultaValorTotal(Locacao locacao){
		Locacao LocacaoConsultado=null;
		String sql="SELECT dbo.calculaValorTotal(?,?,?,?) as valorTotal";
		
		
		try {
			PreparedStatement ps=c.prepareStatement(sql);
			ps.setString(1, locacao.getCliente().getCpf());
			ps.setInt(2, locacao.getAutomovel().getCodAutomovel());
			ps.setString(3, locacao.getDataLocacao());
			ps.setString(4, locacao.getDataDevolucao());
			
			ResultSet rs=ps.executeQuery();
			
			if(rs.next()){
				LocacaoConsultado=new Locacao();
				LocacaoConsultado.setValorTotal(rs.getDouble("valorTotal"));
			}
			rs.close();
			ps.close();
			
			 
			
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, e.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
		}
		
		return LocacaoConsultado;
		
	}
	
	
	
}


