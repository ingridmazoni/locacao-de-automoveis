package dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import pojo.Locacao;

public class SimulacaoDao {
Connection c;
	
	public SimulacaoDao(){
		GenericDao gDao=new GenericDao();
		c=gDao.getConnection();
	}
	
	public Locacao simulaLocacao(Locacao locacao)throws SQLException{
		
		String sql="{call simulacao(?,?,?,?)}";
		Locacao loc=null;
		
			CallableStatement cs=c.prepareCall(sql);
			cs.setString(1, locacao.getCliente().getCpf());
			cs.setInt(2, locacao.getAutomovel().getCodAutomovel());
			cs.setString(3, locacao.getDataLocacao());
			cs.setString(4, locacao.getDataDevolucao());
			ResultSet rs=cs.executeQuery();
					

			while(rs.next()){
				loc=new Locacao();
				loc.setValorTotal(rs.getDouble("valorTotal"));
			}
			
						
			cs.close();
			
		return loc;
		
	}
	
}

