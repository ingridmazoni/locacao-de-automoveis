package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;
import pojo.Cliente;



public class ClienteDao {
Connection c;
	
	public ClienteDao(){
		GenericDao gDao=new GenericDao();
		c=gDao.getConnection();
	}
	
	public boolean insereCliente(Cliente cliente)throws SQLException{
		boolean inserido=false;
		String sql="INSERT INTO cliente VALUES(?,?,?,?,?,?)";
		try{
			PreparedStatement ps=c.prepareStatement(sql);
			ps.setString(1, cliente.getCpf());
			ps.setString(2, cliente.getCnh());
			ps.setString(3, cliente.getNome());
			ps.setString(4, cliente.getEndereco());
			ps.setString(5, cliente.getTelefone());
			ps.setString(6, cliente.getDataNascimento());
			ps.execute();
			ps.close();
			inserido=true;
		}
		catch (SQLException e) {
			
			JOptionPane.showMessageDialog(null, e.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
			
		}
		return inserido;
		
	}
	
	public boolean atualizaCliente(Cliente cliente)throws SQLException{
		boolean atualizado=false;
		String sql="UPDATE cliente set cnh=?,nome=?,endereco=?,telefone=?,dataNascimento=? where cpf=?";
		
			PreparedStatement ps=c.prepareStatement(sql);
			ps.setString(1, cliente.getCnh());
			ps.setString(2, cliente.getNome());
			ps.setString(3, cliente.getEndereco());
			ps.setString(4, cliente.getTelefone());
			ps.setString(5, cliente.getDataNascimento());
			ps.setString(6, cliente.getCpf());
			ps.execute();
			ps.close();
			atualizado=true;
			
			
		
		return atualizado;
		
	}
	
	public boolean excluiCliente(Cliente cliente)throws SQLException{
		boolean excluido=false;
		String sql="DELETE from cliente where cpf=?";
		
		PreparedStatement ps;
		
			ps = c.prepareStatement(sql);
			ps.setString(1, cliente.getCpf());
			ps.execute();
			ps.close();
			excluido=true;
			
		
		
		
		return excluido;
	}
	
	public Cliente consultaCliente(Cliente cliente)throws SQLException{
		Cliente ClienteConsultado=null;
		String sql="select cpf,cnh,nome,endereco,telefone,substring(cast(dataNascimento as varchar(10)),9,2)+'/'+" +
"substring(cast(dataNascimento as varchar(10)),6,2)+'/'+ substring(cast(dataNascimento as varchar(10)),1,4) as dataNascimento from cliente where cpf like ?";
		
		
			PreparedStatement ps=c.prepareStatement(sql);
			ps.setString(1, cliente.getCpf());
			ResultSet rs=ps.executeQuery();
			
			if(rs.next()){
				ClienteConsultado=new Cliente();
				ClienteConsultado.setCpf(rs.getString("cpf"));
				ClienteConsultado.setCnh(rs.getString("cnh"));
				ClienteConsultado.setNome(rs.getString("nome"));
				ClienteConsultado.setEndereco(rs.getString("endereco"));
				ClienteConsultado.setTelefone(rs.getString("telefone"));
				ClienteConsultado.setDataNascimento(rs.getString("dataNascimento"));
				
						
			}
			rs.close();
			ps.close();
			
			 
			
		
		
		return ClienteConsultado;
		
	}
	
	public List<Cliente> consultaListaCliente(){
		List<Cliente> listaCliente=new ArrayList<Cliente>();
		StringBuffer sql=new StringBuffer();
		sql.append("select * from consultaListaCliente");
		
		
		 try {
			PreparedStatement ps=c.prepareStatement(sql.toString());
			ResultSet rs=ps.executeQuery();
			
			while(rs.next()){
				Cliente ClienteConsultado=new Cliente();
				ClienteConsultado.setCpf(rs.getString("cpf"));
				ClienteConsultado.setCnh(rs.getString("cnh"));
				ClienteConsultado.setNome(rs.getString("nome"));
				ClienteConsultado.setEndereco(rs.getString("endereco"));
				ClienteConsultado.setTelefone(rs.getString("telefone"));
				ClienteConsultado.setDataNascimento(rs.getString("dataNascimento"));
				ClienteConsultado.setIdade(rs.getInt("idade"));
				listaCliente.add(ClienteConsultado);
			}
			
			rs.close();
			ps.close();
			
			
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, e.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
		}
			
		 return listaCliente;
		 
		
		
	}
}



