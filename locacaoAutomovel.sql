create database locacaoAutomovel
use locacaoAutomovel


create table tipo(
codTipo int identity not null,
tipo varchar(100)
primary key(codTipo)
)



create table automoveis(
codAutomovel int identity not null,
marca varchar(100),
modelo varchar(100),
cor varchar(100),
ano int,
motor decimal(7,2),
codTipo int,
primary key(codAutomovel),
foreign key(codTipo) references tipo(codTipo)
)


create table cliente(
cpf varchar(14)not null,
cnh varchar(100) not null,
nome varchar(100),
endereco varchar(100),
telefone varchar(14),
dataNascimento date
primary key(cpf)
)


create table locacao(
codAutomovel int not null,
cpf varchar(14) not null,
dataLocacao date,
dataDevolucao date,
valorTotal decimal(7,2)
primary key(codAutomovel,dataLocacao)
foreign key(cpf) references cliente(cpf),
foreign key(codAutomovel) references automoveis(codAutomovel),
check(dataDevolucao>dataLocacao)
)


insert into cliente(cpf,cnh,dataNascimento)values('383.361.928-66',222,'1989-11-32')
insert into cliente(cpf,cnh,dataNascimento)values('383.361.928-75',222,'1989-33-33')
insert into cliente(cpf,cnh,dataNascimento)values('383.361.928-45',222,'1989/08/08')
insert into cliente(cpf,cnh,dataNascimento)values('383.361.928-33',222,'13/08/1989')
insert into cliente(cpf,cnh,dataNascimento)values('383.361.928-33',222,'29/02/1989')

select * from cliente

insert into tipo values('B�sico')
insert into tipo values('Intermedi�rio')
insert into tipo values('Luxo')

select * from tipo



create procedure simulacao2(@cpf varchar(14),@codCarro int,@dataLocacao varchar(10),@dataDevolucao varchar(10)) 
begin
	declare @verificaCarro int
	declare @dateLocacao date
	declare @dateDevolucao date
	declare @valida int
	
	set @valida=0
	set @verificaCarro=(select count(*) from automoveis where codAutomovel=@codCarro)
	set @dateLocacao=cast(@dataLocacao as date)
	set @dateDevolucao=cast(@dataDevolucao as date)
	
	if((select dbo.validaCPF(@cpf))=1)
	begin
	
		if(@verificaCarro=1)
		begin
				if(@dateDevolucao>@dateLocacao)
				begin
					set @valida=1	
			
				end
				else
				begin
					raiserror('Data Invalida',16,1)
				end
		end
	end	
	


end




create function calculaValorTotal(@codTipo int,@dataNasc varchar(10),@anoCarro int,@motor decimal(7,2))
returns decimal(7,2)
as
begin
	declare @tipo varchar(100)
	set @tipo=(select tipo from tipo where codTipo=@codTipo)
	
	declare @dateNasc date
	set @dateNasc=cast(@dataNasc as date)
	
	declare @idade int
	set @idade=cast((datepart(year,getdate())-datepart(year,@dateNasc))as int)
	
	declare @valorTotal decimal(7,2)
	
	set @valorTotal=case @tipo
					when 'B�sico' then
							case @motor
								when 1.6 then  75.00+10.00  
							else
								75.00
							end
					when 'Intermedi�rio' then
							case @motor
								when 2.4 then  150.00+10.00  
							else
								150.00
							end
					when 'Luxo' then
							 case @anoCarro
								when 2012 then  225.00+15.00  
							else
								225
							end
					end
		
	if(@idade>=18 and @idade<=24) or(@idade>=80)
	begin
		set @valorTotal=@valorTotal+10
	end
	
	if(@anoCarro<2010)
	begin
		set @valorTotal=@valorTotal-10	
	end
		
	return @valorTotal
end



create function validaCPF(@cpf varchar(14))
returns int
as
begin
declare @total int
declare @cont int
declare @digito varchar(1)
declare @cont1 int
declare @verificador varchar(1)
declare @cpf2 as varchar(14)
declare @retorno as int
set @cont1=1
set @cont=10
set @total=0
set @cpf2=substring(@cpf,1,11)
set @retorno=0
	 
	while(@cont1<=11)
	begin
		set @digito=substring(@cpf2,@cont1,1)
		
			if(@digito!='.')and(@digito!='-')
			begin
				set @total=@total+(cast(@digito as int)*@cont)
				set @cont=@cont-1
			end
			
	set @cont1=@cont1+1
	end
	


	if(@total%11<2)
	begin 
		set @verificador='0'
	end
	else
	begin 
	
		set @verificador=CAST((11-(@total%11)) as varchar(1))
	end
	
	set @cont1=1
	set @cont=11
	set @total=0
	set @cpf2=RTRIM(@cpf2+'-'+@verificador)
	set @verificador=''
	
	
	while(@cont1<=13)
	begin
		set @digito=substring(@cpf2,@cont1,1)
		
			if(@digito!='.')and(@digito!='-')
			begin
				set @total=@total+(cast(@digito as int)*@cont)
				set @cont=@cont-1
			end
			
	set @cont1=@cont1+1
	end
	
	
	if(@total%11<2)
	begin 
		set @verificador='0'
	end
	else
	begin 
	
		set @verificador=CAST((11-(@total%11)) as varchar(1))
	end
	
	set @cpf2=RTRIM(@cpf2+@verificador)
	
	if(@cpf=@cpf2)
	begin
		set @retorno=1
	end
	
	
	return @retorno
end


create trigger valCpf
on cliente
after insert,update
as
begin
	declare @cpf varchar(14)
	declare @retorno int
	set @cpf=(select cpf from inserted)
	set @retorno=(select dbo.validaCPF(@cpf))
	
	if(@retorno=0)
	begin
		rollback transaction
		raiserror('N�o foi possivel cadastrar cliente pois cpf � inv�lido',16,1)
	
	end
end


insert into cliente(cpf,cnh) values('383.361.928-45','ggg444')
insert into cliente(cpf,cnh) values('383.361.928-75','ggg444')
insert into cliente(cpf,cnh) values('055.285.508-15','ggg444')

insert into cliente(cpf,cnh) values('055.285.508.00','ggg444')

 select dbo.calculaValorTotal(2,'13/08/1990',2009,2.4) as ValorTotal
  select dbo.calculaValorTotal(4,'13/08/1990',2011,2.5) as ValorTotal
  select dbo.validaCPF('383.361.928-15')
  
