create database locacaoAutomovel
use locacaoAutomovel


create table tipo(
codTipo int identity not null,
tipo varchar(100) not null
constraint PK_Tipo primary key(codTipo)
)



create table automoveis(
codAutomovel int identity not null,
marca varchar(100),
modelo varchar(100),
cor varchar(100),
ano int not null,
motor decimal(7,2) not null,
codTipo int not null,
constraint PK_Automoveis primary key(codAutomovel),
constraint FK_Automoveis foreign key(codTipo) references tipo(codTipo)
)


create table cliente(
cpf varchar(14)not null,
cnh varchar(100)not null,
nome varchar(100)not null,
endereco varchar(100),
telefone varchar(14),
dataNascimento date not null,
idade as datediff(year,dataNascimento,getDate())
constraint PK_Cliente primary key(cpf),
constraint UQ_Cliente_CNH unique(cnh)
)


create table locacao(
codAutomovel int not null,
cpf varchar(14) not null,
dataLocacao date not null,
dataDevolucao date not null,
valorTotal decimal(7,2)not null
constraint PK_Locacao primary key(codAutomovel,cpf,dataLocacao)
constraint FK_Locacao_Cliente foreign key(cpf) references cliente(cpf),
constraint FK_Locacao_Automoveis foreign key(codAutomovel) references automoveis(codAutomovel),
constraint CHK_Locacao_Valida_dataDevolucao check(dataDevolucao>dataLocacao)
)




insert into tipo values('B�sico')
insert into tipo values('Intermedi�rio')
insert into tipo values('Luxo')


insert into automoveis values ('Volkswagen','Gol','Prata',2014,1.6,1)
insert into automoveis values ('Volkswagen','Gol','Prata',2014,1.2,1)
insert into automoveis values ('Fiat','Palio','Azul',2009,1.6,1)
insert into automoveis values ('Fiat','Palio','Azul',2009,1.2,1)
insert into automoveis values ('Audi','A6','Prata',2014,2.4,2)
insert into automoveis values ('Audi','A6','Prata',2014,2.2,2)
insert into automoveis values ('Mercedez Benz','Classe A','Prata',2009,2.4,2)
insert into automoveis values ('Mercedez Benz','Classe A','Prata',2009,2.2,2)
insert into automoveis values ('Lamborghini','Diablo','Preto',2012,3.5,3)
insert into automoveis values ('Jaguar','Daimler','Vermelho',2009,3.5,3)
insert into automoveis values ('Rolls Roice','Phantom','Preto',2011,3.5,3)

insert into cliente(cpf,nome,cnh,dataNascimento) values('692.421.658-75','Renato','jjj','13/08/1990')--24 anos
insert into cliente(cpf,nome,cnh,dataNascimento) values('126.367.858-09','Ana','ddd','06/05/1996')--18 anos
insert into cliente(cpf,nome,cnh,dataNascimento) values('477.361.145-60','Bruno','aaa','04/04/1993')--21 anos
insert into cliente(cpf,nome,cnh,dataNascimento) values('386.537.647-98','Vinicius','hhhh','24/12/1989')--25 anos
insert into cliente(cpf,nome,cnh,dataNascimento) values('437.877.368-67','Natasha','uuu','15/09/1997')--17 anos
insert into cliente(cpf,nome,cnh,dataNascimento) values('531.286.228-25','Jessica','wwwww','01/03/1953')--61 anos
insert into cliente(cpf,nome,cnh,dataNascimento) values('985.596.749-69','Paula','qqqqq','02/02/1954')--60 anos
--www.geradorcpf.com




create procedure simulacao(@cpf varchar(14),@codCarro int,@dataLocacao varchar(10),@dataDevolucao varchar(10)) 
as
	
	declare @dateLocacao date
	declare @dateDevolucao date
	set @dateLocacao=cast(@dataLocacao as date)
	set @dateDevolucao=cast(@dataDevolucao as date)
	
		
	declare @verificaCarro int
	set @verificaCarro=(select count(*) from automoveis where codAutomovel=@codCarro)
		
	declare @verificaLocacao int
	set @verificaLocacao=(select COUNT(*) from locacao  where codAutomovel=@codCarro and @dataLocacao between dataLocacao and dataDevolucao)
	
	
	declare @verificaLocacao2 int
	set @verificaLocacao2=(select COUNT(*) from locacao  where codAutomovel=@codCarro and @dataDevolucao between dataLocacao and dataDevolucao)
		
	if((select COUNT(*) from cliente where cpf like @cpf)=0)
	begin
		raiserror('O cliente deve estar cadastrado para fazer a simula��o',16,1)
	end
	else
	begin
			if(@verificaCarro=0)
			begin
				raiserror('O ve�culo deve estar cadastrado para fazer a simula��o',16,1)
						
			end
			else
			begin
				if(@dataDevolucao<@dataLocacao)
				begin
					raiserror('A data de devolu��o deve ser maior que a data de loca��o',16,1)
				
				end
				else
				begin
						if(@verificaLocacao=1 or @verificaLocacao2=1)
						begin
							raiserror('A data de loca��o � inv�lida pois este ve�culo j� esta alugado nesta data',16,1)
						
						
						end
						else
						begin
							select dbo.calculaValorTotal(@cpf,@codCarro,@dataLocacao,@dataDevolucao) as valorTotal
						
						end
					end
				end
			end
	
				


	
	






create function calculaValorTotal(@cpf varchar(14),@codCarro int,@dataLocacao varchar(10),@dataDevolucao varchar(10))
returns decimal(7,2)
as
begin
	declare @tipo varchar(100)
	set @tipo=(select tipo.tipo from tipo inner join automoveis on
		tipo.codTipo=automoveis.codTipo where automoveis.codAutomovel=@codCarro)
	
	
	declare @motor decimal(7,2)
	set @motor=(select motor from automoveis where codAutomovel=@codCarro) 
	
	declare @anoCarro int
	set @anoCarro=(select ano from automoveis where codAutomovel=@codCarro)
	
		
	declare @idade int
	set @idade=(select idade from cliente where cpf like @cpf)
	
	declare @quantDias int
	set @quantDias=datediff(day,cast(@dataLocacao as date),cast(@dataDevolucao as date))
	
	declare @valorTotal decimal(7,2)
	
	set @valorTotal=case @tipo
					when 'B�sico' then
							case @motor
								when 1.6 then  75.00+10.00  
							else
								75.00
							end
					when 'Intermedi�rio' then
							case @motor
								when 2.4 then  150.00+10.00  
							else
								150.00
							end
					when 'Luxo' then
							 case @anoCarro
								when 2012 then  225.00+15.00  
							else
								225
							end
					end
		
	if(@idade>=18 and @idade<=24) or(@idade>60)
	begin
		set @valorTotal=@valorTotal+10
	end
	
	if(@anoCarro<2010)
	begin
		set @valorTotal=@valorTotal-10	
	end
		
	set @valorTotal=@valorTotal*@quantDias
		
	return @valorTotal
end



create procedure cadastraLocacao(@cpf varchar(14),@codCarro int,@dataLocacao varchar(10),@dataDevolucao varchar(10))
as

	declare @valorTotal as decimal(7,2)
	set @valorTotal=(select dbo.calculaValorTotal(@cpf,@codCarro,@dataLocacao,@dataDevolucao) as valorTotal)
	
	
	declare @verificaLocacao int
	set @verificaLocacao=(select COUNT(*) from locacao  where codAutomovel=@codCarro and @dataLocacao between dataLocacao and dataDevolucao)
	
	
	declare @verificaDevolucao int
	set @verificaDevolucao=(select COUNT(*) from locacao  where codAutomovel=@codCarro and @dataDevolucao between dataLocacao and dataDevolucao)
	
		
	if(@verificaLocacao=0)
	begin
		if(@verificaDevolucao=0)
		begin
		
			insert into locacao values(@codCarro,@cpf,cast(@dataLocacao as date),cast(@dataDevolucao as date),@valorTotal)
		end
		else
		begin
			raiserror('A data de devolu��o � inv�lida pois este ve�culo j� esta alugado nesta data',16,1)
		end	
	end
	else
	begin
		raiserror('A data de loca��o � inv�lida pois este ve�culo j� esta alugado nesta data',16,1)
	
	end
	
	
	create procedure atualizaLocacao(@cpf varchar(14),@codCarro int,@dataLocacao varchar(10),@dataDevolucao varchar(10))
	as
	
	declare @valorTotal as decimal(7,2)
	set @valorTotal=(select dbo.calculaValorTotal(@cpf,@codCarro,@dataLocacao,@dataDevolucao) as valorTotal)
	
	declare @verificaDevolucao int
	set @verificaDevolucao=(select COUNT(*) from locacao  where codAutomovel=@codCarro and @dataDevolucao between dataLocacao and dataDevolucao)
	
		
	if( @verificaDevolucao=0)
	begin
		update locacao set dataDevolucao=CAST(@dataDevolucao as date),valorTotal=@valorTotal
		where codAutomovel=@codCarro and dataLocacao=cast(@dataLocacao as date)and cpf like @cpf 
			
	end
	else
	begin
		raiserror('A data de devolu��o � inv�lida pois este ve�culo j� esta alugado nesta data',16,1)
	
	end
	
		




create function validaCPF(@cpf varchar(14))
returns int
as
begin
declare @total int
declare @cont int
declare @digito varchar(1)
declare @cont1 int
declare @verificador varchar(1)
declare @cpf2 as varchar(14)
declare @retorno as int
set @cont1=1
set @cont=10
set @total=0
set @cpf2=substring(@cpf,1,11)
set @retorno=0
	 
	while(@cont1<=11)
	begin
		set @digito=substring(@cpf2,@cont1,1)
		
			if(@digito!='.')and(@digito!='-')
			begin
				set @total=@total+(cast(@digito as int)*@cont)
				set @cont=@cont-1
			end
			
	set @cont1=@cont1+1
	end
	


	if(@total%11<2)
	begin 
		set @verificador='0'
	end
	else
	begin 
	
		set @verificador=CAST((11-(@total%11)) as varchar(1))
	end
	
	set @cont1=1
	set @cont=11
	set @total=0
	set @cpf2=RTRIM(@cpf2+'-'+@verificador)
	set @verificador=''
	
	
	while(@cont1<=13)
	begin
		set @digito=substring(@cpf2,@cont1,1)
		
			if(@digito!='.')and(@digito!='-')
			begin
				set @total=@total+(cast(@digito as int)*@cont)
				set @cont=@cont-1
			end
			
	set @cont1=@cont1+1
	end
	
	
	if(@total%11<2)
	begin 
		set @verificador='0'
	end
	else
	begin 
	
		set @verificador=CAST((11-(@total%11)) as varchar(1))
	end
	
	set @cpf2=RTRIM(@cpf2+@verificador)
	
	if(@cpf=@cpf2)
	begin
		set @retorno=1
	end
	
	
	return @retorno
end


create trigger valCpf
on cliente
after insert,update
as
begin
	declare @cpf varchar(14)
	declare @retorno int
	set @cpf=(select cpf from inserted)
	set @retorno=(select dbo.validaCPF(@cpf))
	
	if(@retorno=0)
	begin
		rollback transaction
		raiserror('N�o foi possivel cadastrar cliente pois cpf � inv�lido',16,1)
	
	end
end



create procedure selecionaLocacao(@codcarro int,@cpf varchar(14),@dataLocacao varchar(10))
as
select locacao.codAutomovel,locacao.cpf,cliente.nome,
substring(cast(locacao.dataLocacao as varchar(10)),9,2)+'/'+
substring(cast(locacao.dataLocacao as varchar(10)),6,2)+'/'+
substring(cast(locacao.dataLocacao as varchar(10)),1,4) as dataLocacao,
substring(cast(locacao.dataDevolucao as varchar(10)),9,2)+'/'+
substring(cast(locacao.dataDevolucao as varchar(10)),6,2)+'/'+
substring(cast(locacao.dataDevolucao as varchar(10)),1,4) as dataDevolucao,
locacao.valorTotal from locacao inner join cliente on locacao.cpf=cliente.cpf 
where locacao.codAutomovel=@codcarro and locacao.cpf like @cpf and locacao.dataLocacao=cast(@dataLocacao as date)


create view selecionaListaLocacao
as
select locacao.codAutomovel,automoveis.marca,automoveis.modelo,automoveis.cor,
automoveis.ano,automoveis.motor,automoveis.codTipo,tipo.tipo,locacao.cpf,cliente.nome,
substring(cast(locacao.dataLocacao as varchar(10)),9,2)+'/'+
substring(cast(locacao.dataLocacao as varchar(10)),6,2)+'/'+
substring(cast(locacao.dataLocacao as varchar(10)),1,4) as dataLocacao,
substring(cast(locacao.dataDevolucao as varchar(10)),9,2)+'/'+
substring(cast(locacao.dataDevolucao as varchar(10)),6,2)+'/'+
substring(cast(locacao.dataDevolucao as varchar(10)),1,4) as dataDevolucao,
locacao.valorTotal from cliente inner join locacao on cliente.cpf=locacao.cpf
inner join automoveis on locacao.codAutomovel=automoveis.codAutomovel 
inner join tipo on automoveis.codTipo=tipo.codTipo
 

create view consultaListaCliente
as
select cpf,cnh,nome,endereco,telefone,
substring(cast(dataNascimento as varchar(10)),9,2)+'/'+
substring(cast(dataNascimento as varchar(10)),6,2)+'/'+
substring(cast(dataNascimento as varchar(10)),1,4) as dataNascimento,idade from cliente


create view consultaListaAutomoveis
as
select automoveis.codAutomovel,automoveis.marca,automoveis.modelo,automoveis.cor,
automoveis.ano,automoveis.motor,automoveis.codTipo,tipo.tipo
from automoveis inner join tipo on automoveis.codTipo=tipo.codTipo


create procedure excluiLocacao(@cpf varchar(14),@codCarro int,@dataLocacao varchar(10))
as
delete from locacao where cpf=@cpf and codAutomovel=@codCarro and dataLocacao=cast(@dataLocacao as date)



select * from cliente
select * from locacao

